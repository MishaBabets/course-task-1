
function main(){
    $.ajax({
        url: "https://randomuser.me/api/?results=10",
      })
    .done(function(data) {
        data.results.forEach(function(element) {
            addRow(element);
            addDetailedRow(element);
          });
    });
}

function addRow(row) {
    var userId = row.login.username;
    var fields = [
        row.name.last, row.name.first, userId,
        row.phone, row.location.city
    ];
    $('#tableBody tr:last').after('<tr id=' + userId + '></tr>');

    $(`#${userId}`).append(
        `<td><div class="circular--landscape"><img src="${row.picture.medium}"/></div></td>`
    );
    fields.forEach(function(element){
        $(`#${userId}`).append(`<td>${element}</td>`);
    });
    $(`#${userId}`).append(
        `<td><button type="button" id="${userId}" class="btn btn-outline-dark">
        <i class="fas fa-plus fa-3x"></i></button></td>`
    );

    clickHandler(userId);
}

function addDetailedRow(row){
    var detailedId = `detailed-${row.login.username}`;
    var fieldsFirst = new Map([
        ["Username", row.login.username],
        ["Registered date", row.registered.date],
        ["Email", row.email],
        ["Address", row.location.street],
    ]);
    var fieldsSecond = new Map([
        ["City", row.location.city],
        ["Zip", row.location.postcode],
        ["Birthday", row.dob.date],
        ["Phone", row.phone],
        ["Cell", row.cell]
    ])
    $('#tableBody tr:last').after(`<tr id="${detailedId}"></tr>`);
    $(`#${detailedId}`).hide();
    if (row.gender == 'male'){
        genderIcon = '<i class="fas fa-male"></i>';
    } else {
        genderIcon = '<i class="fas fa-female"></i>';
    }
    var htmlFirst = `<h1>${row.name.first} ${genderIcon}</h1></div>`;
    fieldsFirst.forEach(function(value, key){
        htmlFirst += `<p><b>${key}:</b> ${value}</p>`
    });
    var htmlSecond = ``;
    fieldsSecond.forEach(function(value, key){
        htmlSecond += `<p><b>${key}:</b> ${value}</p>`
    });
    var avatar = `<div class="circular-large-landscape"><img src="${row.picture.large}"/></div>`;
    $(`#${detailedId}`).append(`<td colspan="3"><div>${htmlFirst}</div></td>`);
    $(`#${detailedId}`).append(`<td colspan="2"><div>${htmlSecond}</div></td>`);
    $(`#${detailedId}`).append(`<td colspan="2"><div>${avatar}</div></td>`);
}


function changeIcon(icon){
    if (icon.hasClass("fa-plus")) {
        icon.addClass("fa-minus").removeClass("fa-plus");
    } else{
        icon.addClass("fa-plus").removeClass("fa-minus");
    }
}

function clickHandler(userId){
    var detailedId = `detailed-${userId}`;
    $(`#${userId} button`).click(function(event) {
        event.stopPropagation();
        var $target = $(event.target);
        var detailedTr = $(`#${detailedId}`)

        if ( detailedTr.is(":hidden")) {
            detailedTr.show();
            var openedTr = $("tr[is_show='yes']");
            var opened = openedTr.attr('id');
            if (opened !== undefined ) {
                var openedId = opened.split('-')[1];
                openedTr.hide();
                openedTr.removeAttr('is_show');
                changeIcon($('button#' + openedId).find('i'));
            };

            detailedTr.attr('is_show', 'yes');
        } else {
            detailedTr.hide();
            detailedTr.removeAttr('is_show');
        }
        changeIcon($target);
    });
}

function searchByName() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    table = document.getElementById("users-table");
    tr = table.getElementsByTagName("tr");
  
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[2];
      if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      } 
    }
  }